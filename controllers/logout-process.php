<?php 
	session_start();
	//remove all session variables;
	session_unset();
	//destroy the location;
	session_destroy();

	header("Location: ../views/login.php")
 ?>