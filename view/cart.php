<?php 
	require "../partials/template.php";
	function get_title(){
		echo "Cart";
	}
	function get_body_contents(){
		require "../controllers/connection.php";

?>
<h1 class="text-center py-5">CART PAGE</h1>
<hr>
<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped table-bordered">
		<thead>
			<tr class="text-center">
				<th>Item</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$total = 0;
				if(isset($_SESSION['cart'])){
					foreach($_SESSION['cart'] as $itemId =>$quantity){
					$item_query = "SELECT * FROM items WHERE id = $itemId";

					$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

					$subtotal = $indiv_item['price']*$quantity;

					$total += $subtotal;
					?>
					<tr>
						<td><?php echo $indiv_item['name'] ?></td>
						<td><?php echo $indiv_item['price'] ?></td>
						<td>
							<span class="spanQ"><?php echo $quantity ?></span>
							<form action="../view/add-to-cart-process.php" method="post" class="d-none">
								<input type="hidden" name="id" value="<?php echo $itemID?>">
								<input type="hidden" name="fromCartPage">
								<input type="number" class="form-control d-none" name="cart" value="<?php echo $quantity ?>" data-id="<?php echo $itemId?>">
							</form>
						</td>
						<td><?php echo number_format($total, 2) ?></td>
						<td>
							<a href="../controllers/remove-from-cart-process.php?id=<?php echo $itemId?>" class="btn btn-danger">Remove from Cart</a>
						</td>
					</tr>
					<?php 
						}
					}
					?>


					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
						<td>
							Total:
						</td>
						<td id="totalPayment">
							<?php echo number_format($total,2,".",",")?>
						</td>
						<td>
							<a href="../controllers/empty-cart-process.php" class="btn btn-danger">empty cart</a>
						</td>
						
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td>
							<form action="../controllers/checkout-process.php" method="POST">
							<input type="hidden" name="totalPayment" value="<?php echo $total?>">
							<input type="hidden" name="cod" value="total">
							<button type="submit" class="btn btn-info">Pay via Cash</button>

							</form>

</td>
<td><div id="paypal-button-container"></div></td>
<td></td>
</tr>
		</tbody>
	</table>
</div>
<script src="../assets/scripts/update-cart.js" type="text/javascript">
	let data = new FormData;
	data.append('totalPayment')


	fetch("../controllers/checkout-process.php", {method: "POST", body: data}).then(res.text()).then(res=>{console.log(res.text())
		.then(res=>res.text())
		console.log(res);
		alert('Transaction completed by' + details.payer.name. given_name);
})


</script>
<script>
	let totalPayment = document.getElementById('totalPayment').textContent.split(',')

	paypal.Buttons({
		createOrder: function(data, actions)
		{
</script>
<?php 
}
?>

