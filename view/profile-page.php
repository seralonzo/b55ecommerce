<?php
   require "../partials/template.php";

   function get_title(){
       echo "Profile Page";
   }
   function get_body_contents(){
       require "../controllers/connection.php";
?>
   <h1 class="text-center py-3">Profile Page</h1>
   <div class="container">
       <div class="row">
           <div class="col-lg-4">
               <h1><?php echo $_SESSION ['user']['firstName'];?> <?= $_SESSION ['user']['lastName'];?></h1>
               <p class="text-center py-3"><?php echo $_SESSION['user']['email']?></p>
           </div>
           <div class="col-lg-4">
               <h3>Address:</h3>
               <ul>
 <?php 
     $address_query = "SELECT * FROM addresses WHERE user_id = $userId ";
     $addresses = mysqli_query($conn, $address_query);
     foreach ($addresses as $indiv_address) {
 ?>
     <li><?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city'] . ", " . $indiv_address['zipCode'] ?>
     </li>
   <?php
       }

    ?>
</ul>
<form action="../controllers/add-address-process.php" method="POST">
   <div class="form-group">
       <label for="address1">Address 1:          
       </label>
       <input type="text" name="address1" class="form-control">
  </div>
   <div class="form-group">
       <label for="address2">Address 2:          
       </label>
       <input type="text" name="address2" class="form-control">
   </div>
    <div class="form-group">
       <label for="city">City:           
       </label>
       <input type="text" name="city" class="form-control">
   </div>
   <div class="form-group">
       <label for="zipCode">zipCode:           
       </label>
       <input type="text" name="zipCode" class="form-control">
   </div>
   <input type="hidden" name="user_id" value="<?php echo $userId?>">
   <button class="btn btn-secondary" type="submit">Add Address</button>
   
</form>




<!--  <img class="border" height= "100px" src="" alt="">
<form action="">
   <div class="form-group">
       <input type="file" name="image" class="form-control">
       <button submit="submit" class="btn btn-info">upload</button>
   </div>
</form> -->
</div>
<div class="col-lg-4">
<h3>Contact:</h3>
<ul>
   <?php 
    }

    ?>